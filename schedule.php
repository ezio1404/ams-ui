<?php
 include 'model/dbhelper.php';
 if(!$_SESSION){
    header("location: index.php?m='Please login first'");
}
$profList = getAllProf();
$courseList = getAllCourse();
 $schedList=getAllSched();
?>
<!DOCTYPE html>
<html>

<head>
    <?php include 'head.php'?>
</head>

<body>
    <!--  -->
    <!-- NAVBAR -->
    <?php include 'header.php'; ?>

    <main>
        <!-- start -->
        <div class="section" style="margin: 2rem;">
            <!-- Modal Trigger -->
            <a class="waves-effect waves-light btn modal-trigger" href="#modal1"><i
                    class="material-icons right">add</i>Schedule</a>
        </div>
        <div class="section" style="margin: 2rem;">
            <?php
                if($_GET['status']=="success_delete"){echo "<div class='card-panel teal lighten-5 teal-text'>Deleted sucessfully</div>";}
                if($_GET['status']=="success_adding"){echo "<div class='card-panel green lighten-5 green-text'>Added sucessfully</div>";}
                if($_GET['status']=="failed_adding"){echo "<div class='card-panel red lighten-5 red-text'>Failed adding</div>";}
                if($_GET['status']=="success_updating"){echo "<div class='card-panel green lighten-5 green-text'>Update sucessfully</div>";}
                if($_GET['status']=="failed_updating"){echo "<div class='card-panel red lighten-5 red-text'>Failed updating</div>";}
        ?>
            <table id="example" class="display" style="width:100%">
                <thead>
                    <tr>
                        <!-- concat intructor name -->
                        <th>Subject Code</th>
                        <th>Descriptive Title</th>
                        <th>Insturctor</th>
                        <th>Section</th>
                        <th>Units</th>
                        <th>Time</th>
                        <th>Schedule</th>
                        <th>Room</th>
                        <!-- concat time start and end -->
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach($schedList as $schedData){
                    ?>
                    <tr>
                        <td><?php echo $schedData['subject_code']?></td>
                        <td><?php echo $schedData['desc_title']?></td>
                        <td><?php echo $schedData['profLastname'].", ".$schedData['profFirstname']?></td>
                        <td><?php echo $schedData['section']?></td>
                        <td><?php echo $schedData['units']?></td>
                        <td><?php echo $schedData['time_start']."-".$schedData['time_end']?>
                        <td><?php 
                        $scheduleList= $schedData['sched'];
                        print_r($scheduleList);
                        // foreach($scheduleList as $key){
                        //     echo $key;
                        // }
                        ?>
                        </td>
                        <td><?php echo $schedData['room']?></td>
                        <td>
                            <a class="waves-effect waves-light btn"
                                href="editSched.php?id=<?php echo $schedData['sched_uid']?>"><i
                                    class="material-icons">create</i></a>
                            <button onclick="test(<?php echo $schedData['sched_uid']?>)"
                                class="waves-effect waves-light red btn"><i class="material-icons">delete</i></button>
                        </td>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>

        </div>
        <!-- end -->
    </main>
    <!-- modal -->


    <!-- Modal Structure -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <h4>Schedule</h4>
            <div class="row">
                <form action="controller/schedCtrl.php" method="POST">
                    <p>Basic Info</p>
                    <div class="input-field col s12">
                        <select name="course_uid">
                            <option value="" disabled selected>Choose course</option>
                            <?php
                                foreach($courseList as $courseData){
                            ?>
                            <option value="<?php echo $courseData['course_uid']?>?">
                                <?php echo $courseData['subject_code']." - ".$courseData['desc_title']?> </option>
                            <?php
                                }
                            ?>
                        </select>
                        <label>Course</label>
                    </div>
                    <div class="input-field col s12">
                        <select name="prof_uid">
                            <option value="" disabled selected>Choose instructors</option>
                            <?php
                                foreach($profList as $profData){
                            ?>
                            <option value="<?php echo $profData['prof_uid']?>?">
                                <?php echo $profData['profLastname'].", ".$profData['profFirstname']?> </option>
                            <?php
                                }
                            ?>
                        </select>
                        <label>Instructors</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="time_start" class="timepicker" name="time_start" type="text" class="validate">
                        <label for="time_start">Time Start</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="time_end" class="timepicker" name="time_end" type="text" class="validate">
                        <label for="time_end">Time End</label>
                    </div>
                    <div class="col s12 center">
                    <label for="">Schedule</label>
                        <p>
                            <label style="margin-right :1em;">
                                <input type="checkbox" name="sched_list[]" value="Monday">
                                <span>Monday</span>
                            </label>
                            <label >
                                <input type="checkbox" name="sched_list[]" value="Tuesday">
                                <span>Tuesday</span>
                            </label>
                            <label >
                                <input type="checkbox" name="sched_list[]" value="Wednesday">
                                <span>Wednesday</span>
                            </label>
                            <label >
                                <input type="checkbox" name="sched_list[]" value="Thursday">
                                <span>Thursday</span>
                            </label>
                            <label >
                                <input type="checkbox" name="sched_list[]" value="Friday">
                                <span>Friday</span>
                            </label>
                            <label >
                                <input type="checkbox" name="sched_list[]" value="Saturday">
                                <span>Saturday</span>
                            </label>
                            <label >
                                <input type="checkbox" name="sched_list[]" value="Sunday">
                                <span>Sunday</span>
                            </label>
                        </p>
                    </div>
                    <div class="input-field col s12">
                        <input id="room" name="room" type="text" class="validate">
                        <label for="room">Room</label>
                    </div>
            </div>

        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-red  red white-text btn-flat">Cancel</a>
            <button id="btnAddBldg" class="btn waves-effect waves-light" type="submit" name="btnAddSched">Add
                Schedule
                <i class="material-icons right">add</i>
            </button>
        </div>
        </form>

    </div>
    <!-- modal end -->

    <!--  -->
    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!-- additional js -->
    <script type="text/javascript" src="js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="js/util.js"></script>
    <!-- dataTable js -->
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.buttons.min.js"></script>
    <script src="js/buttons.print.min.js"></script>
    <script src="js/buttons.flash.min.js"></script>
    <script src="js/buttons.html5.min.js"></script>
    <script src="js/jszip.min.js"></script>
    <script src="js/pdfmake.min.js"></script>
    <script src="js/vfs_fonts.js"></script>
    <!-- custom js -->
    <script>
        $(document).ready(function () {
            $('.modal').modal();
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable({
                // items per page
                "pageLength": 20,
                dom: 'Bfrtip',
                buttons: [{
                        extend: 'copy',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6]
                        }
                    },

                    {
                        extend: 'csv',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6]
                        }
                    },

                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6]
                        }
                    },

                    {
                        extend: 'pdf',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6]
                        }
                    }
                ]
            });
        });
    </script>
    <script>
        function test(id) {
            Swal.fire({
                title: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    window.location.href = `controller/deleteSchedule.php?id=${id}`;
                }
            })

        }
    </script>
</body>

</html>