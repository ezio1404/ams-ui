<!DOCTYPE html>
<html>

<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />
    <!-- additional css -->
    <link type="text/css" rel="stylesheet" href="css/sweetalert2.min.css" media="screen,projection" />
    <!-- Custom Css -->
    <link type="text/css" rel="stylesheet" href="css/custom.css" media="screen,projection" />
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Attendance Management System</title>
</head>

<body>
    <nav>
        <div class="nav-wrapper white navbar-fixed">
            <a href="#" class="brand-logo grey-text text-darken-2" ><i class="material-icons">home</i>Attendance Management System</a>
        </div>
        
    </nav>
    <div class="CIT-image">
    <div class="container">
        <div class="section  right card-panel z-depth-4"
            style="border-radius: 20px;margin-top: 10%;margin-right: -15%;padding: 50px 75px;">
            <div class="container">
                
            </div>
            <form action="controller/login.php" method="POST">
                <div class="input-field col s12">
                    <input id="email" type="text" name="email">
                    <label class="active" for="email">Email</label>
                </div>
                <div class="input-field col s12">
                    <input id="password" type="password" name="password">
                    <label class="active" for="password">Password</label>
                    Forgot password?<i><a href="forgotpassword.php">Click here</a></i>
                </div>
                    <button id="btnLogin" name="btnLogin" class="btn waves-effect waves-light btn-large"
                        style="border-radius: 20px;padding: 0px 100px;" type="submit" name="action">Login
                        <i class="material-icons right">send</i>
                    </button>
            </form>
           
    </div>
</div>


    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!-- additional js -->
    <script type="text/javascript" src="js/sweetalert2.min.js"></script>
    <!-- custom js -->

</body>

</html>