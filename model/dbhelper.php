<?php
session_start();

function dbconn(){
    try{
        return new PDO("mysql:hostname=localhost;dbname=ams_db","root","");
    }catch(PDOException $e){
        echo $e->getMessage();
    }
}

function destroy(){
    return null;
}

function login($username, $password){
    $dbconn = dbconn();
    $sql = "SELECT * FROM tbl_users where userUsername = ?  AND userPassword = ?";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute(array($username,$password));
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    $dbconn = destroy();
    return $row;
}


function addCourse($data){
    $dbconn=dbconn();
    $sql="INSERT INTO tbl_course(subject_code,desc_title,section,units) VALUES(?,?,?,?)";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute($data);
    $dbconn=destroy();
}

function addProf($data){
    $dbconn=dbconn();
    $sql="INSERT INTO tbl_prof(profFirstname,profLastname,profCardId,department) VALUES(?,?,?,?)";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute($data);
    $dbconn=destroy();
}
function addSched($data){
    $dbconn=dbconn();
    $sql="INSERT INTO tbl_schedule(course_uid,prof_uid,time_start,time_end,sched,room) VALUES(?,?,?,?,?,?)";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute($data);
    $dbconn=destroy();
}
function addLogs($data){
    $dbconn = dbconn();
    $sql = "INSERT INTO tbl_logs(sched_uid, date, logs_status) VALUES (?,CURRENT_DATE,?)";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute($data);
    $dbconn=null;

}

function updateCurrentLog($data){
    $dbconn=dbconn();
    $sql="UPDATE tbl_logs SET time_out = CURRENT_TIMESTAMP, logs_status = ? WHERE logs_id = ?";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute($data);
    $dbconn=destroy();
}

// -retrive

function selectIfInstructorHasLog($schedUid){
    $dbconn = dbconn();
    $sql = "SELECT * from tbl_logs l, tbl_schedule s, tbl_prof p where p.prof_uid = s.prof_uid AND s.sched_uid = l.sched_uid AND s.sched_uid = ? AND l.date = CURRENT_DATE AND s.time_start <= CURRENT_TIME AND s.time_end >= CURRENT_TIME";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute(array($schedUid));
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    $dbconn = null;
    return $row;
}
function updateSched($data){
    $dbconn=dbconn();
    $sql="UPDATE tbl_schedule SET course_uid=? , prof_uid=?,time_start=?,time_end=?,sched=? ,room=? WHERE sched_uid = ?";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute($data);
    $dbconn=destroy();
}




function selectClass($cardId){

    $dbconn = dbconn();
    $sql = "SELECT * FROM tbl_schedule s,tbl_prof p , tbl_course c WHERE s.prof_uid = p.prof_uid AND p.profCardId = ? AND s.course_uid = c.course_uid  AND s.time_start - INTERVAL 15 MINUTE <= CURRENT_TIME AND s.time_end >= CURRENT_TIME
    ";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute(array($cardId));
    $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn = null;
    return $row;
}

function checkOldPassword($data){
    $dbconn = dbconn();
    $sql = "SELECT COUNT(*) FROM tbl_users where userId = ? AND userPassword = ?";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute($data);
    $row = $stmt->fetchColumn();
    $dbconn = destroy();
    return $row;
}
function getAllCourse(){
    $dbconn=dbconn();
    $sql="SELECT * FROM tbl_course";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute();
    $rows=$stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn=destroy();
    return $rows;
}

function getAllSched(){
    $dbconn=dbconn();
    $sql="SELECT * FROM tbl_schedule sch,tbl_course crs,tbl_prof prf WHERE crs.course_uid = sch.course_uid AND prf.prof_uid = sch.prof_uid";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute();
    $rows=$stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn=destroy();
    return $rows;
}
function getSched($id){
    $dbconn=dbconn();
    $sql="SELECT * FROM tbl_schedule sch,tbl_course crs,tbl_prof prf WHERE crs.course_uid = sch.course_uid AND prf.prof_uid = sch.prof_uid AND sch.sched_uid = ?";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute(array($id));
    $row=$stmt->fetch(PDO::FETCH_ASSOC);
    $dbconn=destroy();
    return $row;
}

function getAllProf(){
    $dbconn=dbconn();
    $sql="SELECT *,tbl_prof.dateCreated AS prof_dateCreated FROM tbl_prof";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute();
    $rows=$stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn=destroy();
    return $rows;
}

function getEmail($data){
    $dbconn = dbconn();
    $sql = "SELECT * FROM tbl_users where userUsername =?";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute(array($data));
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    $dbconn = destroy();
    return $row;
}
function getAllLogs(){
    $dbconn=dbconn();
    $sql="SELECT *,CONCAT(profLastname,',',profFirstName)AS prof_name FROM tbl_logs l, tbl_course c, tbl_prof p, tbl_schedule s WHERE l.sched_uid = s.sched_uid AND p.prof_uid = s.prof_uid AND c.course_uid = s.course_uid";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute();
    $rows=$stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn=destroy();
    return $rows;
}
// ---------- Old ref
function getAllPerson2(){
    $dbconn=dbconn();
    $sql="SELECT idno,fname,lname,email,status FROM tbl_person";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute();
    $rows=$stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn=destroy();
    return $rows;
}

function getPerson($data){
    $dbconn=dbconn();
    $sql="SELECT * FROM tbl_person WHERE idno = ?";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute($data);
    $row =$stmt->fetch();
    $dbconn=destroy();
    return $row;
}

function updatePerson($data){
    $dbconn=dbconn();
    $sql="UPDATE tbl_person SET fname=? , lname=?,email=?,password=?,status=? WHERE idno = ?";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute($data);
    $dbconn=destroy();
}
function updatePassword($data){
    $dbconn=dbconn();
    $sql="UPDATE tbl_users SET userPassword=?  WHERE userId = ?";
    $stmt=$dbconn->prepare($sql);
    $ok=$stmt->execute($data);
    $dbconn=destroy();
    return $ok;
}


function updateStatus($data){
    $dbconn=dbconn();
    $sql="UPDATE tbl_person SET status=? WHERE idno = ?";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute($data);
    $dbconn=destroy();
}

function deleteInstructor($id){
    $isDeleted = false;
    
    $dbconn=dbconn();
    $sql="DELETE FROM tbl_prof WHERE prof_uid = ?";
    $stmt=$dbconn->prepare($sql);
    if($stmt->execute(array($id))){
        $isDeleted = true;
    }
    $dbconn=destroy();
    return $isDeleted;
}

function deleteCourse($id){
    $isDeleted = false;
    
    $dbconn=dbconn();
    $sql="DELETE FROM tbl_course WHERE course_uid = ?";
    $stmt=$dbconn->prepare($sql);
    if($stmt->execute(array($id))){
        $isDeleted = true;
    }
    $dbconn=destroy();
    return $isDeleted;
}
function deleteSchedule($id){
    $isDeleted = false;
    
    $dbconn=dbconn();
    $sql="DELETE FROM tbl_course WHERE course_uid = ?";
    $stmt=$dbconn->prepare($sql);
    if($stmt->execute(array($id))){
        $isDeleted = true;
    }
    $dbconn=destroy();
    return $isDeleted;
}