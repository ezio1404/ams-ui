<?php

include  '../model/dbhelper.php';


$username=$_POST['email'];
$password=$_POST['password'];

$currentUser = login($username, $password);

if((!empty($username) && $username==$currentUser["userUsername"])&&(!empty($password) && $password==$currentUser["userPassword"])){
    
    $_SESSION['id'] = $currentUser['userId'];
    $_SESSION['userName'] = $currentUser['userName'];
    $_SESSION['userType'] = $currentUser['userType'];
    
    header("location:../dashboard.php");
}
else{
    header("location:../index.php?failed_login");
}