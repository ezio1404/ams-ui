<?php
include '../model/dbhelper.php';


if(isset($_POST['btnAddProf'])){
    $profFirstname= htmlentities($_POST['profFirstname']);
    $profLastname= htmlentities($_POST['profLastname']);
    $profCardId= htmlentities($_POST['profCardId']);
    $dept= htmlentities($_POST['dept']);

    $data=array($profFirstname,$profLastname,$profCardId,$dept);
    $flag=true;

    foreach($data as $d){
        if(empty($d)){
            $flag=false;
            break;
        }
    }

    if($flag){
        addProf($data);
        header("Location:../instructor.php?status=successInstructor");
    }
    else{
        echo "<script> alert('Error Adding') </script>";
        header("Location:../instructor.php?status=failedInstructor");
    }

}