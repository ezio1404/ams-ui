<?php
include '../model/dbhelper.php';
if(isset($_POST['update'])){
    $sched_uid= htmlentities($_POST['sched_uid']);
    $course_uid= htmlentities($_POST['course_uid']);
    $prof_uid= htmlentities($_POST['prof_uid']);
    $time_start= htmlentities($_POST['time_start']);
    $time_end= htmlentities($_POST['time_end']);
    $scheduleList= ($_POST['sched_list']);
    $room= htmlentities($_POST['room']);
    $schedule=array();

    foreach($scheduleList as $selected) {
        array_push($schedule,$selected);
    }
        // print_r($schedule);
        $schedule= implode(",",$schedule);
     

    $data=array($course_uid,$prof_uid,$time_start,$time_end,$schedule,$room,$sched_uid);
    $flag=true;

    foreach($data as $d){
        if(empty($d)){
            $flag=false;
            break;
        }
    }

    
    if($flag){
        updateSched($data);
        header("Location:../schedule.php?status=success_updating");
    }
    else{
        echo "<script> alert('Error Adding') </script>";
        header("Location:../schedule.php?status=failed_updating");
    }

}