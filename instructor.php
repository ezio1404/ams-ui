<?php
 include 'model/dbhelper.php';
 if(!$_SESSION){
    header("location: index.php?m='Please login first'");
}

 $profList=getAllProf();
?>
<!DOCTYPE html>
<html>

<head>
<?php include 'head.php'?>
</head>

<body>
    <!--  -->
    <!-- NAVBAR -->
    <?php include 'header.php'; ?>

    <main>
        <!-- start -->
        <div class="section" style="margin: 2rem;">
            <!-- Modal Trigger -->
            <a class="waves-effect waves-light btn modal-trigger" href="#modal1"><i
                    class="material-icons right">add</i>Instructors</a>
        </div>
        <div class="section" style="margin: 2rem;">
        <?php
                if($_GET['status']=="success_delete"){echo "<div class='card-panel teal lighten-5 teal-text'>Deleted sucessfully</div>";}
                if($_GET['status']=="successInstructor"){echo "<div class='card-panel green lighten-5 green-text'>Added sucessfully</div>";}
                if($_GET['status']=="failedInstructor"){echo "<div class='card-panel red lighten-5 red-text'>An error Occured</div>";}
        ?>
    
            <table id="example" class="display" style="width:100%">
                <thead>
                    <tr>
                        <!-- concat intructor name -->
                        <th>Card ID</th>
                        <th>Name</th>
                        <th>Department</th>
                        <!-- concat time start and end -->
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach($profList as $profData){
                    ?>
                    <tr>
                        <td><?php echo $profData['profCardId']?></td>
                        <td><?php echo $profData['profLastname'].','.$profData['profFirstname']?></td>
                        <td><?php echo $profData['department']?></td>
                        <td>
                            <button onclick="test(<?php echo $profData['prof_uid']?>)"
                                class="waves-effect waves-light red btn"><i class="material-icons">delete</i></button>
                        </td>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>

        </div>
        <!-- end -->
    </main>
    <!-- modal -->
    <!-- Modal Structure -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <h4>Instructor</h4>
            <div class="row">
                <form action="controller/instructorCtrl.php" method="POST">
                    <p>Basic Info</p>
                    <div class="input-field col s12">
                        <input id="profCardId" name="profCardId" type="text" class="validate">
                        <label for="profCardId">Card ID</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="profFirstname" name="profFirstname" type="text" class="validate">
                        <label for="profFirstname">First Name</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="profLastname" name="profLastname" type="text" class="validate">
                        <label for="profLastname">Last Name</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="dept" name="dept" type="text" class="validate">
                        <label for="dept">department</label>
                    </div>

            </div>

        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-red  red white-text btn-flat">Cancel</a>
            <button id="btnAddBldg" class="btn waves-effect waves-light" type="submit" name="btnAddProf">Add
                Instructor
                <i class="material-icons right">add</i>
            </button>
        </div>
        </form>

    </div>
    <!-- modal end -->

    <!--  -->
    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!-- additional js -->
    <script type="text/javascript" src="js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="js/util.js"></script>
    <!-- dataTable js -->
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.buttons.min.js"></script>
    <script src="js/buttons.print.min.js"></script>
    <script src="js/buttons.flash.min.js"></script>
    <script src="js/buttons.html5.min.js"></script>
    <script src="js/jszip.min.js"></script>
    <script src="js/pdfmake.min.js"></script>
    <script src="js/vfs_fonts.js"></script>
    <!-- custom js -->
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable({
                // items per page
                "pageLength": 10,
                dom: 'Bfrtip',
                buttons: [{
                        extend: 'copy',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    },

                    {
                        extend: 'csv',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    },

                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    },

                    {
                        extend: 'pdf',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    }
                ]
            });
        });
    </script>
    <script>
        function test(id) {
            Swal.fire({
                title: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if(result.value){
                    window.location.href = `controller/deleteInstructor.php?id=${id}`;
                }
            })

        }
    </script>
</body>

</html>