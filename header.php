
<header>
        <nav>
            <div class="nav-wrapper  white navbar-fixed">
                <div class="row">
                    <div class="col s12">
                        <a id="burger-icon-1" href="#" data-target="sidebar-1"
                            class="left sidenav-trigger show-on-medium grey-text text-darken-2"><i
                                class="material-icons">menu</i></a>
                        <a href="#" class="brand-logo grey-text text-darken-2"><i
                                class="material-icons">home</i>Attendance Manangement System</a>
                    </div>
                </div>
            </div>
        </nav>


        <!-- SIDEBAR 1 - HAS .SIDENAV-FIXED -->
        <ul id="sidebar-1" class="sidenav sidenav-fixed">
            <li>
                <!-- Put the user data here -->
                <div class="user-view">
                    <div class="background">
                        <img src="https://i.ytimg.com/vi/s9KqVej3VXQ/maxresdefault.jpg">
                    </div>
                    <a href="#!user"><img class="circle" src="http://placehold.it/512x512/333"></a>
                    <a href="#!name"><span class="white-text name"><?php echo $_SESSION['userName'] ?></span></a>
                    <a href="#!email"><span class="white-text email"></span></a>
                </div>
                <!-- Put the user data here Eof-->
            </li>
            <?php
            if($_SESSION['userType'] == 1){
                echo "<li ><a href='dashboard.php?status='><i class='material-icons'>format_align_justify</i>Attendance
                Record</a></li>
                <li><a href='instructor.php?status='><i class='material-icons'>person_outline</i> Instructors</a></li>
                <li><a href='course.php?status='><i class='material-icons'>domain</i> Course</a></li>
                <li><a href='schedule.php?status='><i class='material-icons'>event</i> Schedule</a></li>
                ";
            }
            else{
                echo "<li ><a href='dashboard.php?status='><i class='material-icons'>format_align_justify</i>Attendance
                Record</a></li>
                <li><a href='schedule.php?status='><i class='material-icons'>event</i> Schedule</a></li>";
            }
            
            ?>
            <li><a href="controller/logout.php?status="><i class="material-icons">logout</i>Signout</a></li>
        </ul>
    </header>