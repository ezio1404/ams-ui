<?php
include 'model/dbhelper.php';

if(!$_SESSION){
    header("location: index.php?m='Please login first'");
}
$logsList=getAllLogs();

?>
<!DOCTYPE html>
<html>

<head>
<?php include 'head.php' ?>
</head>

<body>
    <!--  -->
    <!-- NAVBAR -->
    <?php include 'header.php'; ?>

    <main>
        <!-- start -->
        
        <div class="section" style="margin: 2rem;">


            <table id="example" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th>Code - Section</th>
                        <th>Professor</th>
                        <th>Descriptive title</th>
                        <th>Units</th>
                        <!-- concat time start and end and days-->
                        <th>Schedule</th>
                        <th>Days</th>
                        <th>Room</th>
                        <th>Time in</th>
                        <th>Time out</th>
                        <th>Status</th>
                        <!-- <th>Action</th> -->
                    </tr>
                </thead>
                <tbody>
                <?php
                        foreach($logsList as $logsData){
                    ?>
                    <tr>
                        <td><?php echo $logsData['subject_code'].'-'.$logsData['section']?></td>
                        <!-- sql query concat lanme and fname -->
                        <td><?php echo $logsData['prof_name']?></td>
                        <td><?php echo $logsData['desc_title']?></td>
                        <td><?php echo $logsData['units']?></td>
                        <td><?php echo $logsData['time_start'].'-'.$logsData['time_end']?></td>
                        <td><?php echo $logsData['sched']?></td>
                        <td><?php echo $logsData['room']?></td>
                        <td><?php echo $logsData['time_in']?></td>
                        <td><?php echo $logsData['time_out']?></td>
                        <td><?php echo $logsData['logs_status']?></td>
                        
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>        
            </table>

        </div>

        <!-- end -->
    </main>
    <!-- modal -->
    <!-- Modal Structure -->

    <!-- modal end -->

    <!--  -->
    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!-- additional js -->
    <script type="text/javascript" src="js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="js/util.js"></script>
    <!-- dataTable js -->
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.buttons.min.js"></script>
    <script src="js/buttons.print.min.js"></script>
    <script src="js/buttons.flash.min.js"></script>
    <script src="js/buttons.html5.min.js"></script>
    <script src="js/jszip.min.js"></script>
    <script src="js/pdfmake.min.js"></script>
    <script src="js/vfs_fonts.js"></script>
    <!-- custom js -->
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            var elems = document.querySelectorAll('.datepicker');
            var instances = M.Datepicker.init(elems,{
                defaultDate: new Date(),
                setDefaultDate: true
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable({
                // items per page
                "pageLength": 10,
                dom: 'Bfrtip',
                buttons: [{
                        extend: 'copy',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    },

                    {
                        extend: 'csv',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    },

                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    },

                    {
                        extend: 'pdf',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    }
                ]
            });
        });
    </script>
</body>

</html>