<?php

include 'model/dbhelper.php';
date_default_timezone_set('Asia/Manila');

$Tarrive = mktime(01,30,00);
$TimeArrive = date("H:i:s");
$dateNow = date("Y-m-d");

$currentTime = date("H:i:s");

$currentDay = date('l');



//$cardId = $_GET['CardID'];

$cardId = 15347865;
$class = selectClass($cardId);
$room = '123';


if(!empty($cardId) && !empty($class)){

    $days = array();
    foreach($class as $c){
        $dates = $c['sched'];
        $arrayDates = array($dates);
        $scheArray = explode(",",$dates);
        print_r($scheArray);
        if($room === $c['room']){
            foreach($scheArray as $s){
                if($currentDay === $s){
                    $logs = selectIfInstructorHasLog($c['sched_uid']);
                    
                    if(empty($logs)){ //check if instructors has logs
                        $classUid = $c['sched_uid'];
                        $timeForLogin = $c['time_start'];
                        $timeEndForLogin = date('H:i:s', strtotime("+15 minutes", strtotime($timeForLogin)));
                        $timeStartForLogin = date('H:i:s', strtotime("-15 minutes", strtotime($timeForLogin)));
                        if($currentTime >= $c['time_start'] && $currentTime <= $timeEndForLogin){
                            $logstatus = "Arrived on Time";
                        }
                        else if($currentTime >= $timeStartForLogin && $currentTime <= $timeForLogin){
                            $logstatus = "Arrived Early";
                        }
                        else{
                            $logstatus = "Arrived Late";
                        }
    
                        $data=array($classUid, $logstatus);
                        addLogs($data);
                    }
                    else{ //time out
                        $logId = $logs['logs_id'];
                        $logStatus = $logs['logs_status'];
                        if(NULL === $logs['time_out']){
                            if($currentTime === $logs['time_end']){
                                $logStatus .= " and Logged out on time";
                            }
                            else if($currentTime < $logs['time_end']){
                                $logStatus .= " and Logged out early";
                            }
                            else{
                                $logStatus .= " and Logged out late";
                            }
    
                            $data=array($logStatus, $logId);
                            updateCurrentLog($data);
                        }
                        
                        else{
                            echo 'You have already timed out';
                        }
                    }
                }
                else{
                    echo "No schedule for this time".'<br>';
                }
            }
        }
        else{
            echo "Wrong room";
        }
        
    }
    
}else{
    echo 'No sched in this ID';
}


?>