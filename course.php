<?php
 include 'model/dbhelper.php';
 if(!$_SESSION){
    header("location: index.php?m='Please login first'");
}

 $courseList=getAllCourse();
?>
<!DOCTYPE html>
<html>

<head>
    <?php include 'head.php'?>
</head>

<body>
    <!--  -->
    <!-- NAVBAR -->
    <?php include 'header.php'; ?>

    <main>
        <!-- start -->
        <div class="section" style="margin: 2rem;">
            <!-- Modal Trigger -->
            <a class="waves-effect waves-light btn modal-trigger" href="#modal1"><i
                    class="material-icons right">add</i>Course</a>
        </div>
        <div class="section" style="margin: 2rem;">
            <table id="example" class="display" style="width:100%">
                <thead>
                    <tr>
                        <!-- concat intructor name -->
                        <th>Subject Code</th>
                        <th>Descriptive Title</th>
                        <th>Section</th>
                        <th>Units</th>
                        <!-- concat time start and end -->
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach($courseList as $courseData){
                    ?>
                    <tr>
                        <td><?php echo $courseData['subject_code']?></td>
                        <td><?php echo $courseData['desc_title']?></td>
                        <td><?php echo $courseData['section']?></td>
                        <td><?php echo $courseData['units']?></td>
                        <td>
                            <button onclick="test(<?php echo $courseData['course_uid']?>)"
                                class="waves-effect waves-light red btn"><i class="material-icons">delete</i></button>
                        </td>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>

        </div>
        <!-- end -->
    </main>
    <!-- modal -->
    <!-- Modal Structure -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <h4>Course</h4>
            <div class="row">
                <form action="controller/courseCtrl.php" method="POST">
                    <p>Basic Info</p>
                    <div class="input-field col s12">
                        <input id="subject_code" name="subject_code" type="text" class="validate">
                        <label for="subject_code">Subject Code</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="desc_title" name="desc_title" type="text" class="validate">
                        <label for="desc_title">Descriptive Title</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="section" name="section" type="text" class="validate">
                        <label for="section">Section</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="units" name="units" type="number" min="0" class="validate">
                        <label for="units">Units</label>
                    </div>
            </div>

        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-red  red white-text btn-flat">Cancel</a>
            <button id="btnAddBldg" class="btn waves-effect waves-light" type="submit" name="btnAddCourse">Add
                Course
                <i class="material-icons right">add</i>
            </button>
        </div>
        </form>

    </div>
    <!-- modal end -->

    <!--  -->
    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!-- additional js -->
    <script type="text/javascript" src="js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="js/util.js"></script>
    <!-- dataTable js -->
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.buttons.min.js"></script>
    <script src="js/buttons.print.min.js"></script>
    <script src="js/buttons.flash.min.js"></script>
    <script src="js/buttons.html5.min.js"></script>
    <script src="js/jszip.min.js"></script>
    <script src="js/pdfmake.min.js"></script>
    <script src="js/vfs_fonts.js"></script>
    <!-- custom js -->
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable({
                // items per page
                "pageLength": 10,
                dom: 'Bfrtip',
                buttons: [{
                        extend: 'copy',
                        exportOptions: {
                            columns: [0, 1, 2, 3]
                        }
                    },

                    {
                        extend: 'csv',
                        exportOptions: {
                            columns: [0, 1, 2, 3]
                        }
                    },

                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: [0, 1, 2, 3]
                        }
                    },

                    {
                        extend: 'pdf',
                        exportOptions: {
                            columns: [0, 1, 2, 3]
                        }
                    }
                ]
            });
        });
    </script>
    <script>
        function test(id) {
            Swal.fire({
                title: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    window.location.href = `controller/deleteCourse.php?id=${id}`;
                }
            })

        }
    </script>
</body>

</html>