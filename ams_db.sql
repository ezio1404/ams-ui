-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 02, 2019 at 04:19 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ams_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_class`
--

CREATE TABLE `tbl_class` (
  `class_uid` int(11) NOT NULL,
  `class_code` varchar(255) NOT NULL,
  `class_section` varchar(255) NOT NULL,
  `class_description` varchar(255) NOT NULL,
  `class_unit` double NOT NULL,
  `class_start` time NOT NULL,
  `class_end` time NOT NULL,
  `class_sched` varchar(255) NOT NULL,
  `class_roomNumber` varchar(10) NOT NULL,
  `prof_uid` int(11) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_class`
--

INSERT INTO `tbl_class` (`class_uid`, `class_code`, `class_section`, `class_description`, `class_unit`, `class_start`, `class_end`, `class_sched`, `class_roomNumber`, `prof_uid`, `dateCreated`) VALUES
(2, 'sad', 'sad', 'sad', 1, '00:00:00', '01:00:00', 'ads', '123', 9, '2019-08-12 16:00:06');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_course`
--

CREATE TABLE `tbl_course` (
  `course_uid` int(11) NOT NULL,
  `subject_code` varchar(255) NOT NULL,
  `desc_title` varchar(255) NOT NULL,
  `section` varchar(255) NOT NULL,
  `units` int(11) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_course`
--

INSERT INTO `tbl_course` (`course_uid`, `subject_code`, `desc_title`, `section`, `units`, `dateCreated`) VALUES
(2, 'CODE', 'CODSHITSRASDASDAS', '2', 3, '2019-08-26 17:49:00'),
(4, 'itel php', 'php shit', 'kamunggay', 3, '2019-08-27 14:19:28');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_logs`
--

CREATE TABLE `tbl_logs` (
  `logs_id` int(11) NOT NULL,
  `sched_uid` int(11) NOT NULL,
  `time_in` timestamp NULL DEFAULT current_timestamp(),
  `time_out` timestamp NULL DEFAULT NULL,
  `date` date NOT NULL,
  `logs_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_prof`
--

CREATE TABLE `tbl_prof` (
  `prof_uid` int(11) NOT NULL,
  `profFirstname` varchar(255) NOT NULL,
  `profLastname` varchar(255) NOT NULL,
  `profCardId` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_prof`
--

INSERT INTO `tbl_prof` (`prof_uid`, `profFirstname`, `profLastname`, `profCardId`, `department`, `dateCreated`) VALUES
(11, 'EJ ANTON ', 'POTOT', '15387467', 'IT', '2019-08-26 17:50:34'),
(12, 'VINCE GERALD', 'DELACERNA', '15387468', 'IT', '2019-08-26 17:50:49'),
(13, 'WARREN SVENT', 'LIMPAG', '15347865', 'CRIM', '2019-08-26 17:51:05'),
(14, 'DENNIS', 'DURANO', '5412396', 'IT', '2019-08-26 17:51:16'),
(15, 'SHERYL', 'SATORRE', '4231658', 'IT', '2019-08-26 17:51:39');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_schedule`
--

CREATE TABLE `tbl_schedule` (
  `sched_uid` int(11) NOT NULL,
  `course_uid` int(11) NOT NULL,
  `prof_uid` int(11) NOT NULL,
  `time_start` time NOT NULL,
  `time_end` time NOT NULL,
  `sched` varchar(255) NOT NULL,
  `room` varchar(255) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_schedule`
--

INSERT INTO `tbl_schedule` (`sched_uid`, `course_uid`, `prof_uid`, `time_start`, `time_end`, `sched`, `room`, `dateCreated`) VALUES
(8, 2, 12, '20:30:00', '22:00:00', 'Monday,Wednesday,Friday', '514', '2019-09-02 13:07:42'),
(9, 2, 12, '21:00:00', '22:00:00', 'Tuesday,Thursday', '123', '2019-09-02 13:07:32'),
(10, 2, 13, '22:10:00', '22:30:00', 'Tuesday', '123', '2019-09-02 14:09:37');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `userId` int(11) NOT NULL,
  `userName` varchar(255) NOT NULL,
  `userUsername` varchar(255) NOT NULL,
  `userPassword` varchar(255) NOT NULL,
  `userType` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`userId`, `userName`, `userUsername`, `userPassword`, `userType`) VALUES
(1, 'User 1', 'user', 'admin123', 1),
(2, 'Working 1 ', 'working', 'vincegwapo123', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_class`
--
ALTER TABLE `tbl_class`
  ADD PRIMARY KEY (`class_uid`);

--
-- Indexes for table `tbl_course`
--
ALTER TABLE `tbl_course`
  ADD PRIMARY KEY (`course_uid`);

--
-- Indexes for table `tbl_logs`
--
ALTER TABLE `tbl_logs`
  ADD PRIMARY KEY (`logs_id`);

--
-- Indexes for table `tbl_prof`
--
ALTER TABLE `tbl_prof`
  ADD PRIMARY KEY (`prof_uid`);

--
-- Indexes for table `tbl_schedule`
--
ALTER TABLE `tbl_schedule`
  ADD PRIMARY KEY (`sched_uid`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_class`
--
ALTER TABLE `tbl_class`
  MODIFY `class_uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_course`
--
ALTER TABLE `tbl_course`
  MODIFY `course_uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_logs`
--
ALTER TABLE `tbl_logs`
  MODIFY `logs_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `tbl_prof`
--
ALTER TABLE `tbl_prof`
  MODIFY `prof_uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tbl_schedule`
--
ALTER TABLE `tbl_schedule`
  MODIFY `sched_uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
