<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_USER_NOTICE);
include 'model/dbhelper.php';
 if(!$_SESSION){
    header("location: index.php?m='Please login first'");
}
$course = getSched($_GET['id']);
// print_r($course);

$profList = getAllProf();
$courseList = getAllCourse();

?>
<!DOCTYPE html>
<html>

<head>
    <?php include 'head.php'?>
</head>

<body>
    <!--  -->
    <!-- NAVBAR -->
    <?php include 'header.php'; ?>

    <main>
        <!-- start -->
        <div class="section" style="margin: 2rem;">
            <!-- Modal Trigger -->

        </div>
        <div class="section" style="margin: 2rem;">
            <form action="controller/editSched.php" method="post">
                <div class="row">
                    <div class="input-field col s12">
                        <input type="number" name="sched_uid" id="sched_uid" hidden
                            value="<?php echo $course['sched_uid']?>">
                        <select name="course_uid">
                            <?php

                                foreach($courseList as $courseData){
                            ?>
                            <option value="<?php echo $courseData['course_uid']?>?"
                                <?php echo ($courseData['course_uid']==$course['course_uid'])? "selected":"";?>>
                                <?php echo $courseData['subject_code']." - ".$courseData['desc_title']?> </option>
                            <?php
                                }
                            ?>
                        </select>
                        <label>Course</label>
                    </div>
                    <div class="input-field col s12">
                        <select name="prof_uid">
                            <?php
                                foreach($profList as $profData){
                            ?>
                            <option value="<?php echo $profData['prof_uid']?>?"
                                <?php echo ($profData['prof_uid']==$course['prof_uid'])? "selected":"";?>>
                                <?php echo $profData['profLastname'].", ".$profData['profFirstname']?> </option>
                            <?php
                                }
                            ?>
                        </select>
                        <label>Instructors</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="time_start" class="timepicker" name="time_start" type="text" class="validate"
                            value="<?php echo $course['time_start']?>">
                        <label for="time_start">Time Start</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="time_end" class="timepicker" name="time_end" type="text" class="validate"
                            value="<?php echo $course['time_end']?>">
                        <label for="time_end">Time End</label>
                    </div>
                    <div class="col s12">
                        <!-- <input id="schedule" name="schedule" type="text" class="validate"
                            value="<?php echo $course['sched']?>"> -->
                        <label for="schedule">Schedule</label>
                        <?php
                            $scheArray = explode(",",$course['sched']);
                            if(in_array("Monday", $scheArray)){
                                $checked1= "checked='checked'";
                            }
                            if(in_array("Tuesday", $scheArray)){
                                $checked2= "checked='checked'";
                            }
                            if(in_array("Wednesday", $scheArray)){
                                $checked3= "checked='checked'";
                            }
                            if(in_array("Thursday", $scheArray)){
                                $checked4= "checked='checked'";
                            }
                            if(in_array("Friday", $scheArray)){
                                $checked5= "checked='checked'";
                            }
                            if(in_array("Saturday", $scheArray)){
                                $checked6= "checked='checked'";
                            }
                            if(in_array("Sunday", $scheArray)){
                                $checked7= "checked='checked'";
                            }
                        ?>
                        <p>
                            <label style="margin-right :35px !important;">
                                <input type="checkbox" name="sched_list[]" <?php echo $checked1;?> value="Monday">
                                <span>Monday  </span>
                            </label>
                            <label style="margin-right :35px !important;">
                                <input type="checkbox" name="sched_list[]" <?php echo $checked2;?> value="Tuesday">
                                <span>Tuesday  </span>
                            </label>
                            <label style="margin-right :35px !important;">
                                <input type="checkbox" name="sched_list[]" <?php echo $checked3;?> value="Wednesday">
                                <span>Wednesday  </span>
                            </label>
                            <label style="margin-right :35px !important;">
                                <input type="checkbox" name="sched_list[]" <?php echo $checked4;?> value="Thursday">
                                <span>Thursday  </span>
                            </label>
                            <label style="margin-right :35px !important;">
                                <input type="checkbox" name="sched_list[]" <?php echo $checked5;?> value="Friday">
                                <span>Friday  </span>
                            </label>
                            <label style="margin-right :35px !important;">
                                <input type="checkbox" name="sched_list[]" <?php echo $checked6;?> value="Saturday">
                                <span>Saturday  </span>
                            </label>
                            <label style="margin-right :35px !important;">
                                <input type="checkbox" name="sched_list[]" <?php echo $checked7;?> value="Sunday">
                                <span>Sunday  </span>
                            </label>
                        </p>
                    </div>
                    <div class="input-field col s12">
                        <input id="room" name="room" type="text" class="validate" value="<?php echo $course['room']?>">
                        <label for="room">Room</label>
                    </div>
                    <div class="input-field col s12">
                        <input class="btn " type="submit" value="Update" name="update" id="update">
                    </div>
                </div>

            </form>
        </div>
        <!-- end -->
    </main>
    <!-- modal -->




    <!--  -->
    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!-- additional js -->
    <script type="text/javascript" src="js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="js/util.js"></script>
    <!-- dataTable js -->
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.buttons.min.js"></script>
    <script src="js/buttons.print.min.js"></script>
    <script src="js/buttons.flash.min.js"></script>
    <script src="js/buttons.html5.min.js"></script>
    <script src="js/jszip.min.js"></script>
    <script src="js/pdfmake.min.js"></script>
    <script src="js/vfs_fonts.js"></script>
    <!-- custom js -->

</body>

</html>